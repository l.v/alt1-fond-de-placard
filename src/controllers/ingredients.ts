import { parseJSONBody, BasicRouter } from '../sketchyframework/mybadexpress';
import { IngredientService } from '../services/ingredients';
import { Injectable } from '../sketchyframework/dependencyinjector';

@Injectable()
export class IngredientsController {
    constructor(
        private server: BasicRouter,
        private ingredientService: IngredientService
    ) {
        this.setupRoutes()
    }

    setupRoutes() {
        this.server.post('/api/ingredient/search', async ({req, res, json}) => {
            const body = await parseJSONBody(req);

            const searchRes = await this.ingredientService.search(body.searchTerm);

            json(searchRes);
        })

        this.server.post('/api/ingredient/add', async ({req, res, ctx}) => {
            const body = await parseJSONBody(req);

            const opRes = this.ingredientService.addIngredient(body.name);

            //TODO (never): handle cases where stuff breaks / is already added
            res.writeHead(200);
            res.end();
        })

        //TODO: the server needs to handle ':identifiers'
        this.server.post('/api/ingredient/get', async ({req, res, json}) => {
            const body = await parseJSONBody(req);

            const ingredientData = await this.ingredientService.getIngredient(body.name);

            if (ingredientData) {
                json(ingredientData);
            } else {
                res.writeHead(404);
                res.end();
            }
        })
    }

    //@route('/todo')
    search() {
    }
}
