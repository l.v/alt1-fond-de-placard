import { Injectable } from '../sketchyframework/dependencyinjector';
import { RecipesRepository } from '../repository/recipes';

@Injectable()
export class RecipesService {
    constructor(private recipesRepo: RecipesRepository) {
    }

    getRecipeByName(name: string) {
        return this.recipesRepo.getOne(name);
    }

    getRecipeById(id: number) {
        return this.recipesRepo.getOneById(id);
    }
    
    addRecipe(name: string, ingredients, category, picture) {
        return this.recipesRepo.insertOne(name, ingredients, category, picture);
    }

    async searchByName(name: string) {
        const res = await this.recipesRepo.findByName(name);
        return res;
    }

    searchByIngredients(ingredients) {
        return this.recipesRepo.findByIngredients(ingredients);
    }
};
