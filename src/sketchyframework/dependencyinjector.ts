import "reflect-metadata";

//a bad global dependency inversion injection container
const globalStore = new Map();
const registry = new Map();

interface DependencyI {
    ctor: Function;
    dependencies: Function[];
}

export function register(classCtor, classInstance) {
    return globalStore.set(classCtor, classInstance)
}

export function get(someClass) {
    return globalStore.get(someClass);
}

export function recursiveInstantiate(someClass) {
    if (registry.has(someClass) || globalStore.has(someClass))
        return;

    const paramTypes = Reflect.getMetadata("design:paramtypes", someClass) || [];
    const deps = paramTypes.filter(paramType => typeof paramType === "function");
    
    console.log("recursiveRegister", someClass, "parameters:", paramTypes, "dependencies:", deps);
    
    registry.set(someClass, {
        ctor: someClass,
        dependencies: deps,
    });

    if (deps.length) {
        console.log("recursively instantiating deps...")
        deps.forEach(recursiveInstantiate);
        console.log("done");
    }

    const args = paramTypes.map(param => {
        if (!globalStore.has(param))
            throw new Error(`dependency ${param} for ${someClass} could not be found in the tree`);

        return globalStore.get(param)
    });

    console.log("instantiating", someClass, "with", args);
    globalStore.set(someClass, new someClass(...args));
}

//this is needed at least to trigger typescript's type metadata magic
export function Injectable(...args) {
    //console.log("@Injectable params", arguments);

    return function actualDecorator(target: any): void {
        /*console.log("@Injectable actual decorator:", target['name']);

        var t = Reflect.getMetadata("design:paramtypes", target);
        console.log("@Injectable actual decorator", t);

        t.forEach(type => {
            console.log(type.name, Reflect.ownKeys(type));
        });
        */
    }
}

/*
export function Inject(
    target: Object,
    propertyKey: string | symbol,
    parameterIndex: number
) {
    console.log("inject meta", {from: target['name'], propertyKey, parameterIndex})    

    var t = Reflect.getMetadata("design:paramtypes", target);
    console.log("@Inject type", t[parameterIndex]);
}

export function ClassWrapper<T extends { new (...args: any[]): {} }>(targetClass: T) {
  return (...args) => {
    console.log(args);
    return new targetClass(...args);
  };
}
*/
