import { IngredientEntity } from "./ingredients";

export class RecipeEntity {
    public id?: string;
    public name: string;

    public ingredients: IngredientEntity[] = [];
}
